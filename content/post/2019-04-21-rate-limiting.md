---
title: Rate Limited
subtitle: "Rate limiting"
date: 2019-04-21
tags: [scalable systems, algorithms]
---

# Leaky bucket
## Advantage
Smooths out bursts, single server, memory efficient
## Disadvantage
Burst of traffic fills up queue, starves recent request

# Fixed window
## Advantage
Recent request not starved
## Disadvantage
Burst at window edges, fills up both previous and next window, has to
handle twice the load

# Sliding log
## Advantage
No boundary condition, tracked for every user, no 'stampede' effect
fixed window
## Disadvantage
Memory hog, calculate summation, does not scale well

# Sliding window
## Advantage
Weighed
## Disadvantage
